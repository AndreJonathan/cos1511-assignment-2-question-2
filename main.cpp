// Name: Andre De Jager | Student Number: 38762447 | COS1511 Assignment 2 | 872356
// Question 2

#include <iostream>
#include <string>
using namespace std;

// A function studentDetails, that prompts the learner to key in
// their personal details name,surname, and schoolName.
void studentDetails(string & nameP, string & surnameP, string & schoolP)
{
    // Prompt for and capture name and surname
    cout << "Please key in your name:" << endl;
    cin >> nameP >> surnameP;
    cin.get(); // removes the newline character left behind by >> so that getline can input up to the next newline character.

    // Prompt for and capture entire school name using getline
    cout << "Please key in the name of your school:" << endl;
    getline(cin, schoolP);
}

// Prompt for and capture a generic mark used by GetMarks function
void promptAndValidateMark(int & markP, string markNameP)
{
    do{
        cout << "Key in your mark for " << markNameP << ":"<< endl;
        cin >> markP;

    } while (markP < 0 || markP > 100); // Keep prompting if mark is not valid ie between 0-100 inclusive
}

// A function getMarks, that prompts the learner to key in a
// mark for each of the six subjects, and validate the marks. Do not accept marks lower than 0 or higher than 100.
void getMarks(int & engMarkP, int & mathMarkP, int & lOMarkP,int & histMarkP, int & compLitMarkP,int & geoMarkP)
{
    // 1 Prompt for and get valid English mark
    promptAndValidateMark(engMarkP, "English");

    // 2 Prompt for and get valid Mathematics mark
    promptAndValidateMark(mathMarkP, "Mathematics");

    // 3 Prompt for and get valid Life Orientation mark
    promptAndValidateMark(lOMarkP, "Life Orientation");

    // 4 Prompt for and get History mark
    promptAndValidateMark(histMarkP, "History");

    // 5 Prompt for and get Computer Literacy mark
    promptAndValidateMark(compLitMarkP, "Computer Literacy");

    // 6 Prompt for and get Geography mark
    promptAndValidateMark(geoMarkP, "Geography");
}

// A function calcAverageYearMark, to calculate and display the average of the 6 Subjects.
// This function should be called just once by main, and should be passed the 6 Subject marks.
float calcAverageYearMark(int engMarkP, int mathMarkP, int lOMarkP,int histMarkP, int compLitMarkP,int geoMarkP)
{
    float averageYearMark = (engMarkP + mathMarkP + lOMarkP + histMarkP + compLitMarkP + geoMarkP) / 6.00;

    return averageYearMark;
}

// A function to return the smaller of 2 numbers
int smallerValue(int value1, int value2)
{
    int smallerValue = value2;

    if(value1 < value2)
    {
        smallerValue = value1;
    }

    return smallerValue;
}

// A function to return the larger of 2 numbers
int largerValue(int value1, int value2)
{
    int largerValue = value2;

    if(value1 > value2)
    {
        largerValue = value1;
    }

    return largerValue;
}

// A function minMax, to find and sey the lowest (minMarkP) and
// the highest (maxMarkP) of the 6 subject marks passed to it as the subject with the lowest mark;
// No arrays or collections to be used as not yet learned
void minMax(int engMarkP, int mathMarkP, int lOMarkP,int histMarkP, int compLitMarkP,int geoMarkP, int & minMarkP, int & maxMarkP)
{
    // FIND MIN MARK
    minMarkP = engMarkP;
    minMarkP = smallerValue(minMarkP, mathMarkP);
    minMarkP = smallerValue(minMarkP, lOMarkP);
    minMarkP = smallerValue(minMarkP, histMarkP);
    minMarkP = smallerValue(minMarkP, compLitMarkP);
    minMarkP = smallerValue(minMarkP, geoMarkP);

    // FIND MAX MARK
    maxMarkP = engMarkP;
    maxMarkP = largerValue(maxMarkP, mathMarkP);
    maxMarkP = largerValue(maxMarkP, lOMarkP);
    maxMarkP = largerValue(maxMarkP, histMarkP);
    maxMarkP = largerValue(maxMarkP, compLitMarkP);
    maxMarkP = largerValue(maxMarkP, geoMarkP);
}

// A function to return a 1 if mark over 50 ie passed used in passOrFail function
int passedOrFailedInt(int mark)
{
    int res = 0;
    if(mark >= 50)
    {
        res = 1;
    }

    return res;
}

// A function passOrFail, to determine whether the student has passed or failed grade 12.
// The learner has to pass at least four subjects, including English, to complete grade 12. The subject pass mark is 50%.
string passOrFail(int engMarkP, int mathMarkP, int lOMarkP,int histMarkP, int compLitMarkP,int geoMarkP)
{
    string passOrFail = "Failed";
    int subjectCounterPass = 0;

    // Count how many subjects passed excluding English
    subjectCounterPass += passedOrFailedInt(mathMarkP);
    subjectCounterPass += passedOrFailedInt(lOMarkP);
    subjectCounterPass += passedOrFailedInt(histMarkP);
    subjectCounterPass += passedOrFailedInt(compLitMarkP);
    subjectCounterPass += passedOrFailedInt(geoMarkP);

    // The learner has to pass at least four subjects, including English ie English and any 3 others minimum
    if(engMarkP >= 50 && subjectCounterPass >= 3)
    {
        passOrFail = "Passed";
    }

    return passOrFail;
}

// A function awardDistinction to determine which of the subjects have received distinctions.
// A subject receives a distinction if the mark is 75% and above.
void awardDistinction (int mark, bool & distinctionP)
{
    distinctionP = false;
    if(mark >= 75 )
    {
        distinctionP = true;
    }
}

// Set symbol and code for passed in mark for a subject
void codeSymbol(int markP, string & theSymbol, int & theCode)
{

    if(markP >= 0 && markP <= 29)
    {
        theCode = 1;
        theSymbol = "FF";
    }
    else if(markP >= 30 && markP <= 39)
    {
        theCode = 2;
        theSymbol = "F ";
    }
    else if(markP >= 40 && markP <= 49)
    {
        theCode = 3;
        theSymbol = "E ";
    }
    else if(markP >= 50 && markP <= 59)
    {
        theCode = 4;
        theSymbol = "D ";
    }
    else if(markP >= 60 && markP <= 69)
    {
        theCode = 5;
        theSymbol = "C ";
    }
    else if(markP >= 70 && markP <= 79)
    {
        theCode = 6;
        theSymbol = "B ";
    }
    else
    {
        theCode = 7;
        theSymbol = "A ";
    }
}

// A function that calculates how many spaces are needed to keep the columns 1 and 2 aligned correctly
string buildSpaces(string wordP, int MAX_SUBJECT_LEN)
{
    string spaces = "";

    int len = wordP.length();
    int spacesRequired = MAX_SUBJECT_LEN - len;
    char const SPC_CHAR = 32;

    for(int i=0; i < spacesRequired;i++)
    {
        spaces += SPC_CHAR;
    }

    return spaces;
}

// A function to output the header
void displayResultHeader(string subjectHeader, string markHeader, string symbolHeader, string codeHeader, int MAX_SUBJECT_LEN)
{
    // Calculate number of spaces needed and build a spacer using char ascii code 32
    string spaces = buildSpaces(subjectHeader, MAX_SUBJECT_LEN);

    // Output header
    cout << subjectHeader << spaces <<"\t" << markHeader << "\t" << symbolHeader << "\t\t" << codeHeader << endl;
}

// A function to output the lines with correct spaces
void displayResultLine(string subjectP, int markP, string symbolP, int codeP, int MAX_SUBJECT_LEN, bool isDisinction)
{

    string distinctionMessage = "";

    //Set/show the distinction message if it is true
    if(isDisinction)
    {
        distinctionMessage = "with distinction";
    }

    // Calculate number of spaces needed and build a spacer using char ascii code 32
    string spaces = buildSpaces(subjectP, MAX_SUBJECT_LEN);

    // Output the line
    cout << subjectP << spaces <<"\t" << markP << "%\t\t  " << symbolP << "\t\t  " << codeP << "\t" << distinctionMessage <<  endl;
}

int main() {

    // Declare variables
    string name, surname, school;
    int engMark, mathMark, lOMark, histMark, compLitMark, geoMark, minMark, maxMark;
    bool engDistinction, mathDistinction, lODistinction, histDistinction, compLitDistinction, geoDistinction;
    string symbolEng, symbolMath, symbolLifeOr, symbolHist, symbolCompLit, symbolGeo, symbolAveYM;
    int codeEng, codeMath, codeLifeOr, codeHist, codeCompLit, codeGeo, codeAveYM;
    float averageYearMark;
    string outcome;
    int const MAX_SUBJECT_LEN = 17; //Used to calculate correct spacing when outputting results

    // Set precision and fixed
    cout.setf(ios::fixed);
    cout.precision(2);

    // Get the student details
    studentDetails(name, surname, school);

    // Get the students marks
    getMarks(engMark, mathMark, lOMark, histMark, compLitMark, geoMark);

    // Calculate the average year mark
    averageYearMark = calcAverageYearMark(engMark, mathMark, lOMark, histMark, compLitMark, geoMark);

    // Set the min mark and set the max mark
    minMax(engMark, mathMark, lOMark, histMark, compLitMark, geoMark, minMark, maxMark);

    // Set the outcome
    outcome = passOrFail(engMark, mathMark, lOMark, histMark, compLitMark, geoMark);

    // Set distinction for subjects where applicable
    awardDistinction(engMark, engDistinction);
    awardDistinction(mathMark, mathDistinction);
    awardDistinction(lOMark, lODistinction);
    awardDistinction(histMark, histDistinction);
    awardDistinction(compLitMark, compLitDistinction);
    awardDistinction(geoMark, geoDistinction);

    // Calculate symbols and codes for each subject
    codeSymbol(engMark,symbolEng,codeEng);
    codeSymbol(mathMark,symbolMath,codeMath);
    codeSymbol(lOMark,symbolLifeOr,codeLifeOr);
    codeSymbol(histMark,symbolHist,codeHist);
    codeSymbol(compLitMark,symbolCompLit,codeCompLit);
    codeSymbol(geoMark,symbolGeo,codeGeo);
    codeSymbol(averageYearMark,symbolAveYM,codeAveYM);

    // Override the outcome if a distinction
    // ... a student has passed with distinction if the average mark is 75% and above.
    if(averageYearMark >= 75)
    {
        outcome = "Passed with distinction";
    }

    // Output heading and message
    cout << "***************************************************************" << endl;
    cout << "                    STUDENT ACADEMIC RECORD                    " << endl;
    cout << "This program inputs the learner marks of matric level subjects \n and prints the student final report." << endl;
    cout << "***************************************************************" << endl;

    // Output full name and school
    cout << "Name: " << name << " " << surname << "\t\t" << "School: " << school << endl;
    cout <<  endl;

    // Output header
    displayResultHeader("SUBJECT", "MARK", "SYMBOL", "CODE",MAX_SUBJECT_LEN);

    // Output lines
    displayResultLine("English", engMark,symbolEng,codeEng,MAX_SUBJECT_LEN, engDistinction);
    displayResultLine("Mathematics", mathMark,symbolMath,codeMath,MAX_SUBJECT_LEN, mathDistinction);
    displayResultLine("Life Orientation", lOMark,symbolLifeOr,codeLifeOr,MAX_SUBJECT_LEN, lODistinction);
    displayResultLine("History", histMark,symbolHist,codeHist,MAX_SUBJECT_LEN, histDistinction);
    displayResultLine("Computer Literacy", compLitMark,symbolCompLit,codeCompLit,MAX_SUBJECT_LEN, compLitDistinction);
    displayResultLine("Geography", geoMark,symbolGeo,codeGeo,MAX_SUBJECT_LEN, geoDistinction);

    cout <<  endl;

    // Output average year mark, symbol and code
    cout << "Average Year Mark: " << averageYearMark << " with Symbol " << symbolAveYM << " and code " << codeAveYM << endl;

    // Output the outcome
    cout << "Outcome: " << outcome << endl;

    cout <<  endl;

    // Output the highest mark
    cout << "The highest mark was " << maxMark << "%"<< endl;
    // Output the lowest mark
    cout << "The lowest mark was " << minMark << "%"<< endl;

    cout << "***************************************************************" << endl;

    return 0;
}